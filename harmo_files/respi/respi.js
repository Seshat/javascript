

document.write('\<div id="maDivCentre">');

var container, stats;
var camera, scene, renderer,projector;
var anim,frame,speed;
var objects = [];

var mouseX = 0, mouseY = 0;

var windowHalfX;
var windowHalfY;

init();
addObjects();
animate();


function init() {

        width = 500;
        height = 500;

          frame = 0;
          speed = 50;

        windowHalfX=width/2;
        windowHalfY=height/2;

        anim="none";
        camera = new THREE.PerspectiveCamera( 45, width / height, 1, 2000 );
        camera.position.z = 300;

        scene = new THREE.Scene();

        var ambient = new THREE.AmbientLight( 0x101030 );
        scene.add( ambient );

        projector = new THREE.Projector();
        var directionalLight = new THREE.DirectionalLight( 0xffeedd );
        directionalLight.position.set( 0, 1, 1 );
        scene.add( directionalLight );
        var directionalLight2 = new THREE.DirectionalLight( 0xffeedd );
        directionalLight2.position.set( 0, 1, -1 );
        scene.add( directionalLight2 );
        var directionalLight3 = new THREE.DirectionalLight( 0xffeedd );
        directionalLight3.position.set( 0, -1, 0 );
        scene.add( directionalLight3 );

        // model

        renderer = new THREE.WebGLRenderer();
        renderer.setSize( width, height );
        renderer.domElement.style.marginTop = '20px';
        renderer.domElement.style.border = '1px solid black';
        renderer.domElement.innerHTML = 'Desol?, votre navigateur ne semble pas supporter WebGL, veuillez en changer';

        controls = new THREE.OrbitControls(camera,renderer.domElement);
        controls.addEventListener( 'change', render );

        document.body.appendChild( renderer.domElement );

        window.addEventListener( 'resize', onWindowResize, false );

}

function onWindowResize() {
        windowHalfX = width / 2;
        windowHalfY = height / 2;

        camera.aspect = width / height;
        camera.updateProjectionMatrix();
        renderer.setSize( width, height );
        render();
}

function onDocumentMouseDown( event ) {

    event.preventDefault();
    alert("Coordonees x : "+(( event.clientX / window.innerWidth ) * 2 - 1) + " - y : " +(- ( event.clientY / window.innerHeight ) * 2 + 1));
    var vector = new THREE.Vector3( ( event.clientX / window.innerWidth ) * 2 - 1,
                                   - ( event.clientY / window.innerHeight ) * 2 + 1,
                                   0.5 );
    projector.unprojectVector( vector, camera );

    var raycaster = new THREE.Raycaster( camera.position, vector.subSelf( camera.position ).normalize() );

    var object = scene.getObjectByName('trach' , true );

    var intersects = raycaster.intersectObject( object );

    //alert("Nombre d'intersections : "+intersects.length);

    if ( intersects.length > 0 ) {

        alert("touche");

    }
}

function addObjects() {
       var loader = new THREE.OBJMTLLoader();
        loader.load( './harmo_files/respi/three/obj/Colonne/Vertebres.obj', './harmo_files/respi/three/obj/Colonne/Vertebres.mtl', function ( object ) {
                object.position.y = - 70;
                object.name = "vertebre";
                scene.add( object );
        } );
       loader.load( './harmo_files/respi/three/obj/Poumons/PoumonsD.obj', './harmo_files/respi/three/obj/Poumons/PoumonsD.mtl', function ( object ) {
                object.position.y = - 70;
                object.name = "poumonsD";
                scene.add( object );
        } );
       loader.load( './harmo_files/respi/three/obj/Poumons/PoumonsG.obj', './harmo_files/respi/three/obj/Poumons/PoumonsG.mtl', function ( object ) {
                object.position.y = - 70;
                object.name = "poumonsG";
                scene.add( object );
        } );
       loader.load( './harmo_files/respi/three/obj/Poumons/Trach.obj', './harmo_files/respi/three/obj/Poumons/Trach.mtl', function ( object ) {
                object.position.y = - 70;
                object.name = "trach";
                scene.add( object );
        } );

        loader.load( './harmo_files/respi/three/obj/Diaphragme/Diaphragme.obj', './harmo_files/respi/three/obj/Diaphragme/Diaphragme.mtl', function ( object ) {
                object.position.y = - 70;
                object.name = "diaph";	       
                scene.add( object );
        } );

         loader.load( './harmo_files/respi/three/obj/Foie/Foie.obj', './harmo_files/respi/three/obj/Foie/Foie.mtl', function ( object ) {
                object.position.y = - 70;
                object.name = "foie";	       
                scene.add( object );
        } );

        //Chargement de toutes les cotes
        //Gauches
        for ( var i = 1; i < 13; i ++ ) {
          var chem = './harmo_files/respi/three/obj/Cotes/left/CG'+i;
          var nomcote = chem+'.obj';
          var matcote = chem+'.mtl';
          loader.load( nomcote, matcote, function ( object ) {
             object.position.y = - 70;
             scene.add( object );
          } );
       }

       //Droites
       for ( var i = 1; i < 13; i ++ ) {
          var chem = './harmo_files/respi/three/obj/Cotes/right/CD'+i;
          var nomcote = chem+'.obj';
          var matcote = chem+'.mtl';
          loader.load( nomcote, matcote, function ( object ) {
             object.position.y = - 70;
             scene.add( object );
          } );
       }

}

function changeOpacity(name,newValue) {
    var val = document.getElementById(name+'quant').value;
    val=val/100;
    var object = scene.getObjectByName( name, true );
    object.traverse( function( node ) {
    if( node.material ) {
       node.material.opacity = val;
       node.material.transparent = true;
       }
    } );
    document.getElementById(name+"range").innerHTML=newValue;
}

function changeGroupOpacity(name,newValue) {
    var val = document.getElementById(name+'quant').value;
    val=val/100;
    for (var i=1; i<13 ; i++) {
       var object = scene.getObjectByName(name+i , true );
          object.traverse( function( node ) {
          if( node.material ) {
             node.material.opacity = val;
             node.material.transparent = true;
             }
          } );
    }
    document.getElementById(name+"range").innerHTML=newValue;
}

function changeTimer(newValue) {
    var val = document.getElementById("speed").value;
    speed = val;
}

/**************************************************************
 *
 *	Fonction d'animation, appel�e selon un cycle
 *	de quelques millisecondes
 *
 **************************************************************/

function animate() {
       requestAnimationFrame( animate );
       //if (anim ){
          //sleep(speed);
          switch(anim){
             case "diaph" : animationDiaph(); frame ++; break ;
             case "rotate" : animationRotate(); frame ++; break;
             case "cotes" : animationCotes(); frame ++; break;
             default : break;
          }
          if(frame >= 51){
             frame = 1;
          }
       //}
       controls.update();
}

/*********************************************************************
 *
 *	ANIMATION DE LA ROTATION STATIQUE DU MODELE
 *
 ********************************************************************/

function animationRotate(){
    var diaph = scene.getObjectByName('diaph' , true );
    var trach = scene.getObjectByName('trach' , true );
    var vert = scene.getObjectByName('vertebre' , true );
    var foie = scene.getObjectByName('foie' , true );
    var poumonsG = scene.getObjectByName('poumonsG' , true );
    var poumonsD = scene.getObjectByName('poumonsD' , true );

    foie.rotation.y += 3.14/180;
    diaph.rotation.y += 3.14/180;
    poumonsD.rotation.y +=  3.14/180;
    poumonsG.rotation.y +=  3.14/180;
    trach.rotation.y +=  3.14/180;
    vert.rotation.y +=  3.14/180;

    for (var i=1; i<13 ; i++) {
       var object = scene.getObjectByName('CD'+i , true );
       var object2 = scene.getObjectByName('CG' + i , true );
       object.rotation.y += 3.14/180;
       object2.rotation.y += 3.14/180;
    }
 }

/**************************************************************
 *
 *	ANIMATION DE LA RESPIRATION DIAPHRAGMATIQUE
 *
 **************************************************************/

function animationDiaph(){
    var diaph = scene.getObjectByName('diaph' , true );
    var foie = scene.getObjectByName('foie' , true );
    var poumonsG = scene.getObjectByName('poumonsG' , true );
    var poumonsD = scene.getObjectByName('poumonsD' , true );
     if (frame <= 25) {
          diaph.scale.y -= 0.0045;
          diaph.position.y += 0.2;

          foie.position.y -= 0.15;
          foie.scale.x -= 0.0005;
          poumonsG.scale.y += 0.0045;
          poumonsD.scale.y += 0.0045;
          poumonsG.scale.x += 0.001;
          poumonsD.scale.x += 0.001;

          poumonsG.position.y -= 0.55;
          poumonsD.position.y -= 0.55;
     }else{
          diaph.scale.y += 0.0045;
          diaph.position.y -= 0.2;

          foie.position.y += 0.15;
          foie.scale.x += 0.0005;

          poumonsG.scale.y -= 0.0045;
          poumonsD.scale.y -= 0.0045;
          poumonsG.scale.x -= 0.001;
          poumonsD.scale.x -= 0.001;

          poumonsG.position.y += 0.55;
          poumonsD.position.y += 0.55;
     }

    for (var i=1; i<13 ; i++) {
       var object = scene.getObjectByName('CD'+i , true );
       var object2 = scene.getObjectByName('CG' + i , true );
       if (frame <= 25) {
          object.rotation.y -= 3.14/1800;
          object2.rotation.y += 3.14/1800;

          object.position.x -= 0.01;
          object2.position.x += 0.01;
       }else{
          object.rotation.y += 3.14/1800;
          object2.rotation.y -= 3.14/1800;

          object.position.x += 0.01;
          object2.position.x -= 0.01;
       }
    }
 }

/**************************************************************
 *
 *	ANIMATION DE LA RESPIRATION COSTALE
 *
 **************************************************************/

function animationCotes(){
    var diaph = scene.getObjectByName('diaph' , true );
    var foie = scene.getObjectByName('foie' , true );
    var poumonsG = scene.getObjectByName('poumonsG' , true );
    var poumonsD = scene.getObjectByName('poumonsD' , true );
     if (frame <= 25) {
          diaph.scale.y -= 0.001;

          foie.position.y -= 0.15;
          foie.scale.x -= 0.0005;
          poumonsG.scale.y += 0.0045;
          poumonsD.scale.y += 0.0045;
          poumonsG.scale.x += 0.001;
          poumonsD.scale.x += 0.001;

          poumonsG.position.y -= 0.55;
          poumonsD.position.y -= 0.55;
     }else{
          diaph.scale.y += 0.001;

          foie.position.y += 0.15;
          foie.scale.x += 0.0005;

          poumonsG.scale.y -= 0.0045;
          poumonsD.scale.y -= 0.0045;
          poumonsG.scale.x -= 0.001;
          poumonsD.scale.x -= 0.001;

          poumonsG.position.y += 0.55;
          poumonsD.position.y += 0.55;
     }

    for (var i=1; i<13 ; i++) {
       var object = scene.getObjectByName('CD'+i , true );
       var object2 = scene.getObjectByName('CG' + i , true );
       if (frame <= 25) {
          object.rotation.y -= 3.14/180;
          object2.rotation.y += 3.14/180;

          object.position.x -= 0.1;
          object2.position.x += 0.1;
       }else{
          object.rotation.y += 3.14/180;
          object2.rotation.y -= 3.14/180;

          object.position.x += 0.1;
          object2.position.x -= 0.1;
       }
    }
 }

function sleep(milliseconds) {
 var start = new Date().getTime();
 for (var i = 0; i < 1e7; i++) {
   if ((new Date().getTime() - start) > milliseconds){
     break;
   }
 }
}

function changeanimtype(type) {
 if (anim == type) {
    anim="none";
    document.getElementById(type+"radio").checked=false;
 }else{
  anim=type;
 }
}

function render() {
      // camera.lookAt( scene.position );
        renderer.render( scene, camera );
        stats.update();
}
document.write('<\div>');

document.write('\
<div id="maDivDroite" style="float:right;clear:right;">\
            <fieldset>\
    <legend> Interaction </legend>\
                <div class="boutons">\
                    Respiration diaphragmatique<input type="radio" name="anim" onclick=\'changeanimtype("diaph")\' id="diaphradio"><br>\
        Respiration costale<input type="radio" name="anim" onclick=\'changeanimtype("cotes")\' id="cotesradio"><br>\
        Rotation du modele (seulement en statique)<input type="radio" name="anim" onclick=\'changeanimtype("rotate")\' id="rotateradio"><br>\
\
                    <hr class="trait_appli">\
\
                    <input type="range" id="diaphquant" onchange=&quot;changeOpacity("diaph",this.value)&quot; min="0" max="100" value="100" />\
                           <span id="diaphrange">100</span>% - Changer l&quot;opacité du diaphragme<br>\
                    <input type="range" id="vertebrequant" onchange=&quot;changeOpacity("vertebre",this.value)&quot; min="0" max="100" value="100" />\
                           <span id="vertebrerange">100</span>% - Changer l&quot;opacité des vertebres<br>\
                    <input type="range" id="trachquant" onchange=&quot;changeOpacity("trach",this.value)&quot; min="0" max="100" value="100" />\
                           <span id="trachrange">100</span>% - Changer l&quot;opacité de la trachée<br>\
                    <input type="range" id="poumonsDquant" onchange=&quot;changeOpacity("poumonsD",this.value)&quot; min="0" max="100" value="100" />\
                           <span id="poumonsD">100</span>% - Changer l&quot;opacité du poumon droit<br>\
                    <input type="range" id="poumonsGquant" onchange=&quot;changeOpacity("poumonsG",this.value)&quot; min="0" max="100" value="100" />\
                           <span id="poumonsG">100</span>% - Changer l&quot;opacité du poumon gauche<br>\
                    <input type="range" id="CDquant" onchange=&quot;changeGroupOpacity("CD",this.value)&quot; min="0" max="100" value="100" />\
                           <span id="CDrange">100</span>% - Changer l&quot;opacité des cotes droites<br>\
                    <input type="range" id="CGquant" onchange=&quot;changeGroupOpacity("CG",this.value)&quot; min="0" max="100" value="100" />\
                           <span id="CGrange">100</span>% - Changer l&quot;opacité des cotes gauches<br>\
                </div>\
            </fieldset>\
            <br><br>\
            \
            <br>\
        </div>\
        ');


