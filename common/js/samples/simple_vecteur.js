(function() {

  window.samples.simple_vecteur = {

    initialize: function(canvas) {
        var scene    = new THREE.Scene();
        var camera   = new THREE.OrthographicCamera( -0.5, 2, 2, -0.5, 1, 1000 );
        var renderer = new THREE.WebGLRenderer();
        renderer.setClearColor(0xEEEEEE, 1);
        renderer.setSize( sample_defaults.width, sample_defaults.height );
        document.body.appendChild(renderer.domElement);

        var radius   = 0.05,
        segments = 64,
        material2 = new THREE.LineBasicMaterial( { color: 0xff0000 } );
        geometry2 = new THREE.CircleGeometry( radius, segments );
        pt=new THREE.Mesh( geometry2, material2 );
        scene.add( pt );
        pt2=new THREE.Mesh( geometry2, material2 );
        pt2.position.x=1;
        pt2.position.y=1;
        scene.add( pt2 );

        var to = new THREE.Vector3( 1., 1., 0 );
        var from = new THREE.Vector3( 0, 0, 0 );
        var direction = to.clone().sub(from);
        var length = direction.length();
        var arrowHelper = new THREE.ArrowHelper(direction.normalize(), from, length, 0x0000ff,0.1,0.1);
        scene.add( arrowHelper);


        camera.position.z = 5;  
        var renderer = new THREE.WebGLRenderer({canvas: canvas, antialias: true});
        renderer.setSize( sample_defaults.width, sample_defaults.height );

      var instance = { active: false };
      function animate() {
        requestAnimationFrame( animate, canvas );
        if(!instance.active || sample_defaults.paused) return;

        renderer.render( scene, camera );
      }

      animate();
      return instance;
    }
  };
})();
