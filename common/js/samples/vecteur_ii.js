(function() {

  window.samples.vecteur_ii = {

    initialize: function(canvas) {
        var scene    = new THREE.Scene();
        var camera   = new THREE.OrthographicCamera( -0.5, 2, 2, -0.5, 1, 1000 );
        var renderer = new THREE.WebGLRenderer();
        renderer.setClearColor(0xEEEEEE, 1);
        renderer.setSize( sample_defaults.width, sample_defaults.height );
        document.body.appendChild(renderer.domElement);

        var all= new THREE.Object3D();
        var to = new THREE.Vector3( 0, 0, 0 );
        var from = new THREE.Vector3( 0.7, 1., 0 );
        var direction = to.clone().sub(from);
        var length = direction.length();
        var arrowHelper = new THREE.ArrowHelper(direction.normalize(), from, length, 0x0000ff,0.1,0.1);
        all.add( arrowHelper);
        var to = new THREE.Vector3( 1.4,0.2,0 );
        var from = new THREE.Vector3( 0.7, 1., 0 );
        var direction = to.clone().sub(from);
        var length = direction.length();
        var arrowHelper = new THREE.ArrowHelper(direction.normalize(), from, length, 0x000000,0.1,0.1);
        all.add( arrowHelper);
        var to = new THREE.Vector3( 1.4,0.2,0 );
        var from = new THREE.Vector3( 0., 0., 0 );
        var direction = to.clone().sub(from);
        var length = direction.length();
        var arrowHelper = new THREE.ArrowHelper(direction.normalize(), from, length, 0x0000ff,0.1,0.1);
        all.add( arrowHelper);

        scene.add(all);

        camera.position.z = 5;  
        var renderer = new THREE.WebGLRenderer({canvas: canvas, antialias: true});
        renderer.setSize( sample_defaults.width, sample_defaults.height );

      var instance = { active: false };
      var incr=0.001;
      function animate() {
        requestAnimationFrame( animate, canvas );
        if(!instance.active || sample_defaults.paused) return;
            all.rotation.y += incr;
            renderer.render( scene, camera );
            if (Math.abs(all.rotation.y)>0.5)
                    incr=-incr;
      }

      animate();
      return instance;
    }
  };
})();
